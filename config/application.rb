require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Rmrss
  class Application < Rails::Application

    def config_for_if_exists(name, env: Rails.env)
      yaml = name.is_a?(Pathname) ? name : Pathname.new("#{paths["config"].existent.first}/#{name}.yml")

      if yaml.exist?
        config_for(name, env: env)
      end
    end

    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0
    config.matrix_registration = config_for_if_exists(:matrix_registration)
    config.matrix_appservice = config_for_if_exists(:matrix_config)

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
  end
end
