Rails.application.routes.draw do
  root "feeds#index"

  resources :feeds do
    resources :entries
  end
  post '/feeds/:id', to: 'feeds#fetch' #POST to fetch entries for a feed
end
