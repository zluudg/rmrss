class Entry < ApplicationRecord
  belongs_to :feed

  validates :title, presence: true, uniqueness: true
  validates :source , presence: true, uniqueness: true
end
