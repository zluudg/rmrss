class Feed < ApplicationRecord
  has_many :entries, dependent: :destroy

  validates :name, presence: true, uniqueness: true
  validates :url, presence: true, uniqueness: true
end
