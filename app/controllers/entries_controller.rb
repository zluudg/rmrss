class EntriesController < ApplicationController
  def create
    @feed = Feed.find(params[:feed_id])
    @entry = @feed.entries.create(entry_params)
  end

  def destroy
    @feed = Feed.find(params[:feed_id])
    @entry = @feed.entries.find(params[:id])
    @entry.destroy
  end

  def update
    @feed = Feed.find(params[:feed_id])
    @entry = @feed.entries.find(params[:id])

    PublishFeedEntriesJob.perform_later([@entry])

    redirect_to feed_path(@feed), status: :see_other
  end

  private
    def entry_params
      params.require(:entry).permit(:title, :source, :published)
    end
end
