class FeedsController < ApplicationController
  def index
    @feeds = Feed.all
    @feed = Feed.new
  end

  def show
    @feed = Feed.find(params[:id])
    @feed_name_stable = @feed.name
  end

  def create
    @feeds = Feed.all
    @feed = Feed.new(feed_params)

    if @feed.save
      redirect_to @feed
    else
      render :index, status: :unprocessable_entity
    end
  end

  def update
    @feed = Feed.find(params[:id])
    @feed_name_stable = @feed.name

    if @feed.update(feed_params)
      redirect_to @feed
    else
      render :show, status: :unprocessable_entity
    end
  end

  def fetch
    @feed = Feed.find(params[:id])
    @feed_name_stable = @feed.name

    FetchFeedEntriesJob.perform_later([@feed])

    render :show
  end

  def destroy
    @feed = Feed.find(params[:id])
    @feed.destroy

    redirect_to root_path, status: :see_other
  end

  private
    def feed_params
      params.require(:feed).permit(:name, :url)
    end
end
