class FetchFeedEntriesJob < ApplicationJob
  queue_as :default

  def perform(feeds)
    feeds.each do |feed|
      URI.open(feed.url) do |rss|
        f = RSS::Parser.parse(rss)
        if 'rss' == f.feed_type
          get_title_lambda = ->(item) { item.title }
          get_source_lambda = ->(item) { item.link }
        elsif 'atom' == f.feed_type
          get_title_lambda = ->(item) { item.title.content }
          get_source_lambda = ->(item) { item.link.href }
        else
          raise Exception.new "Unsupported feed type!"
        end
        f.items.each do |item|
          title = get_title_lambda.call(item)
          source = get_source_lambda.call(item)
          feed.entries.create(
            title: title,
            source: source,
            published: false) unless Entry.exists?(source: source)
        end
      end
    end
  end

end
