class PublishFeedEntriesJob < ApplicationJob
  queue_as :default

  def perform(entries)
    entries.each do |entry|
      if entry.published
        next # skip if entry has already been published
      end
      entry.update(published: true)
    end
  end
end
