namespace :matrix do

  desc "Generate config and registration files for matrix appservice."
  task :generate do
    Rake::Task['matrix:gen_config'].execute
    Rake::Task['matrix:gen_registration'].execute
  end

  desc "Generate config file for matrix appservice."
  task :gen_config do
    yaml = Pathname.new("config/matrix_config.yml")

    unless yaml.exist?
      File.open(yaml, "w") {}
    end
  end

  desc "Generate registration file for matrix appservice."
  task :gen_registration do
    yaml = Pathname.new("config/matrix_registration.yml")
    reg = "id: rmrss \n"\
          "url: http://localhost:29319\n"\
          "as_token: #{SecureRandom.urlsafe_base64(64)}\n"\
          "hs_token: #{SecureRandom.urlsafe_base64(64)}\n"\
          "sender_localpart: _rmrss_bot\n"\
          "rate_limited: false\n"\
          "namespaces: \n"\
          "  users: \n"\
          "  - exclusive: true\n"\
          "    regex: '@rmrssbot:example\\.com'\n"\
          "  - exclusive: true\n"\
          "    regex: '@rmrss_.*:example\\.com'\n"\
          "  aliases: \n"\
          "  - exclusive: true\n"\
          "    regex: '#rmrss_.*:example\\.com'\n"\
          "  rooms: []"

    unless yaml.exist?
      File.open(yaml, "w") do |file|
        file.write(Psych.parse_stream(reg).to_yaml)
      end
    end
  end

end
